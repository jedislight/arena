// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function metrics_increment(key, amount){
	if (ds_map_exists(global.metrics, key))
		global.metrics[? key] += amount;
	else
		global.metrics[? key] = amount;
}