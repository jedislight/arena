// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.creature_registry = ds_map_create();
global.creature_name_to_object_index = ds_map_create(); 
function CreatureUpgradeHistory() constructor {
    upgrade_types = [];
	upgrade_values = [];
	upgrade_skills = [];
	skill = noone; // working variable;
	
	static apply = function(creature, limit) {
		for(var i = 0; i < min(limit, array_length(upgrade_types)); ++i){
			var upgrade_type = asset_get_index(upgrade_types[i]);
			var upgrade_value = upgrade_values[i];
			var upgrade_skill_object_index = asset_get_index(upgrade_skills[i]);
			var upgrade_skill = noone;
			for(var j = 1; j < array_length(creature.skills); ++j){
				skill = creature.skills[j];
				if (instance_exists(skill) and skill.object_index == upgrade_skill_object_index) {
					upgrade_skill = skill;	
				}
			}
			if (upgrade_skill == noone){
				upgrade_skill = upgrade_skill_object_index;
			}	
			var n = instance_create_depth(0,0,0, upgrade_type);
			var upgrade_script = n.upgrade_script;
			skill = upgrade_skill;
			script_execute(upgrade_script, creature, upgrade_value, upgrade_skill);
			instance_destroy(n);
			creature.stars += 1;
		}
    }
	
	static append = function(upgrade_type, upgrade_value, upgrade_skill){
		array_push(upgrade_types, object_get_name(upgrade_type));
		array_push(upgrade_values, upgrade_value);
		var skill_name;
		{
			if (instance_exists(upgrade_skill)) skill_name = object_get_name(upgrade_skill.object_index);
			else skill_name = object_get_name(upgrade_skill);
		}
		array_push(upgrade_skills, skill_name);
	}
}