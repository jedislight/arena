// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function array_sample(array){
	return array[irandom_range(0, array_length(array)-1)];
}