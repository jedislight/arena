// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_sample_skill(creature){
	if (is_undefined(variable_instance_get(creature, "skills"))){
		return noone;	
	}
	var skills = [];
	for (var i = 1; i < array_length(creature.skills);  ++i){
		var skill = creature.skills[i];
		if (instance_exists(skill)){
			array_push(skills, skill);	
		}
	}
	
	if (array_length(skills) == 0) return noone;
	return array_sample(skills);
}