// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function skill_begin_casting(caster, skill, xx, yy){
	caster.casting = true;
	caster.casting_x = xx;
	caster.casting_y = yy;
	caster.casting_skill_index = array_find(caster.skills, skill);
	var casting_time_ticks = seconds_to_ticks(skill.casting_time_seconds);
	with(caster) alarm_set(Creature_Alarms.CASTING, casting_time_ticks);
	if(casting_time_ticks == 0) {
		with(caster) event_perform(ev_alarm, 0);
		with(caster) alarm_set(Creature_Alarms.CASTING, -1);
	} else {
		if (object_exists(skill.casting_fx_object_index)){
			var n = instance_create_depth(caster.x, caster.y, 0, skill.casting_fx_object_index)	
			n.caster = caster;
			n.casting_time_seconds = skill.casting_time_seconds;
		}
	}
}