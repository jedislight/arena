// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ds_map_to_string(map){
	var text = "";
	var keys_array = ds_map_keys_to_array(map);
	var keys_list = ds_list_create();
	for(var i = 0; i < array_length(keys_array); ++i){
		ds_list_add(keys_list, keys_array[i]);
	}
	
	ds_list_sort(keys_list, true);
	
	for(var i = 0; i < ds_list_size(keys_list); ++i){
		if (i != 0)
			text += "\n"	
		text += string(keys_list[| i]) + "= ";
		text += string(map[? keys_list[|i]]);
	}
	
	ds_list_destroy(keys_list);
	return text;
}