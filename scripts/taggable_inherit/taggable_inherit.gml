// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function taggable_inherit(dest, source){
	array_merge(dest.tags, source.tags);
	array_merge(dest.resistances, source.resistances);
	array_merge(dest.forbidden, source.forbidden);
	array_merge(dest.vulnerabilities, source.vulnerabilities);
}