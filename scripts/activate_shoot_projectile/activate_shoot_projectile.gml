// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function activate_shoot_projectile(caster, xx, yy){
	if (creature_is_casting(caster)) {
		var projectile = instance_create_depth(caster.x, caster.y,0, projectile_object_index);
		if(projectile_uses_facing)
			with (projectile) motion_add(caster.image_angle, other.projectile_speed)
		else
			with (projectile) move_towards_point(xx, yy, other.projectile_speed);
		skill_faction_assign(projectile, caster); 
		taggable_inherit(projectile.id, id);
		projectile.damage = damage;
		projectile.knockback = knockback;
		projectile.damage_duration_seconds = damage_duration_seconds;
		if(radius != 0){
			var projectile_size = (projectile.sprite_height + projectile.sprite_width) / 2;	
			var scale = radius / projectile_size * 2;
			projectile.image_xscale = scale;
			projectile.image_yscale = scale;
		
			if (projectile_speed == 0)
				projectile.image_angle = caster.image_angle;
		}
		skill_activated(id, caster);
	} else if (skill_ready(id, xx, yy)) {
		skill_begin_casting(caster, id, xx, yy);
	}
	
}