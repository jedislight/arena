// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function upgrade_skill_cooldown(creature, value){
	var value_real = 1.0 - (value / 100);
	skill.cooldown_seconds *= value_real;
}