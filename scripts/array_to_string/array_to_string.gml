// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function array_to_string(array){
	var s = "[ ";
	
	for(var i = 0 ; i < array_length(array); ++i){
		if (i == 0)
			s += string(array[i]);
		else
			s += ", " + string(array[i])
	}
	
	s += " ]"
	
	return s;
}