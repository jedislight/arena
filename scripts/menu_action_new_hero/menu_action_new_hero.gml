// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function menu_action_new_hero(){
	with(MatchPlanner) instance_destroy(id);
	with(Creature) instance_destroy(id);
	instance_create_depth(0,0,0, MatchPlanner);
}