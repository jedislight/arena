// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function upgrade_grant_skill(creature, value){
	var slot = 1;
	for (; slot < array_length(creature.skills) and instance_exists(creature.skills[slot]); ++slot){}
	
	creature.skills[slot] = instance_create_depth(0,0,0, skill);
}