// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function faction_assign(object, faction){
	if (faction < 0) {
		faction = irandom_range(-999999, -1);	
	}
	
	object.faction = faction;
}