// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_get_object_index_from_name(name){
	var i = global.creature_name_to_object_index[? name];
	if (is_undefined(i)){
		i = tag_get_assets(["BASE_CREATURE"])[0];
		i = asset_get_index(i);
	}
	return i;
}