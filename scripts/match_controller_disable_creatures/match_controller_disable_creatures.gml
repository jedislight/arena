// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function match_controller_disable_creatures(){
	with(Projectile) instance_destroy(id);
	with (Creature) {
		if (temporary){
			instance_destroy(id);	
		}
		else {
			movement_behavior = noone;
			skill_behavior = noone;
			move_to_x = x;
			move_to_y = y;
			dot_damages = [];
			dot_duration_ticks = [];
			hp = max(1, hp);
		}
	}
}