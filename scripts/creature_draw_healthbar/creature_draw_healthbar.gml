// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_draw_healthbar(left, top, right, bottom, creature){
	var current = creature.hp;
	var maximum = creature.defense;
	var expected = creature.hp - array_sum(creature.dot_damages);
	var current_color = c_red;
	var expected_color = c_purple;
	if (expected > current)
		expected_color = c_green;
	var current_percent = current / maximum * 100;
	var expected_percent = expected / maximum * 100;
	if (expected > current) {
		draw_healthbar(left, top, right, bottom, expected_percent, false, expected_color, expected_color, 0, false, false);
		draw_healthbar(left, top, right, bottom, current_percent, false, current_color, current_color, 0, false, false);
	} else {
		draw_healthbar(left, top, right, bottom, current_percent, false, expected_color, expected_color, 0, false, false);
		draw_healthbar(left, top, right, bottom, expected_percent, false, current_color, current_color, 0, false, false);
	}
	
	var text = creature.name + " (" + string(creature.stars)+ ")" +"\n"+string(ceil(creature.hp))+" / "+string(ceil(creature.defense));
	if ((bottom - top) >= string_height_ext(text, -1, right-left))
		draw_text(left, top, text);

}