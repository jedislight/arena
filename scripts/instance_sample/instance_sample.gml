// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function instance_sample(object_index){
	return instance_find(object_index, irandom_range(0, instance_number(object_index)-1));
}