// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function upgrade_skill_damage(creature, value){
	var value_real = value / 100;
	skill.damage *= 1 + value_real;
}