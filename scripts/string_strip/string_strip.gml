// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function string_strip(s){
	while(string_char_at(s, 1) == " ") {
		s = string_copy(s, 2, string_length(s)-1);	
	}
	while(string_char_at(s, string_length(s)) == "\n") {
		s = string_copy(s, 1, string_length(s)-1);	
	}
	while(string_char_at(s, string_length(s)) == "\r") {
		s = string_copy(s, 1, string_length(s)-1);	
	}
	while(string_char_at(s, string_length(s)) == " ") {
		s = string_copy(s, 1, string_length(s)-1);	
	}
	
	return s;
}