// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function behavior_skill_qwer(){
	with(skills[1]){
		if (keyboard_check(ord("Q"))) {
			script_execute(activate_script, other.id, mouse_x, mouse_y);
		}
	}
	with(skills[2]){
		if (keyboard_check(ord("W"))) {
			script_execute(activate_script, other.id, mouse_x, mouse_y);
		}
	}
	with(skills[3]){
		if (keyboard_check(ord("E"))) {
			script_execute(activate_script, other.id, mouse_x, mouse_y);
		}
	}
	with(skills[4]){
		if (keyboard_check(ord("R"))) {
			script_execute(activate_script, other.id, mouse_x, mouse_y);
		}
	}
}