// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function activate_ether_tear(caster, xx, yy){
	if (creature_is_casting(caster)) {
		var dir = point_direction(caster.x, caster.y, xx, yy);
		var distance = min(radius, point_distance(caster.x, caster.y, xx, yy));
		var clamped_x = caster.x + lengthdir_x(distance, dir);
		var clamped_y = caster.y + lengthdir_y(distance, dir);
	
		var old_x = caster.x;
		var old_y = caster.y;
	
		caster.x = clamped_x;
		caster.y = clamped_y;
	
		caster.move_to_x = caster.x;
		caster.move_to_y = caster.y;
	
		var line = instance_create_depth(old_x, old_y, 0, EtherTearArea);
		taggable_inherit(line, id);
		var scale = distance / line.sprite_width;
		line.image_xscale = scale;
		line.image_yscale = scale;
		line.image_angle = dir;
		line.piercing = 9999;
		line.damage_duration_seconds = damage_duration_seconds;
		line.damage = damage;
		skill_faction_assign(line, caster); 
	
		skill_activated(id, caster);		
	} else if (skill_ready(id, xx, yy)) {
		skill_begin_casting(caster, id, xx, yy);
	}
}