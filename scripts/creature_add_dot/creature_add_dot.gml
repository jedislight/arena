// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_add_dot(creature, source, damage, duration_seconds){
	if (creature_resists(creature, source.tags))
		damage *= 0.5;
	if(creature_vulnerable(creature, source.tags))
		damage *= 2.0;
	array_push(creature.dot_damages, damage);
	array_push(creature.dot_duration_ticks, seconds_to_ticks(duration_seconds));
}