// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ticks_to_seconds(ticks){
	return real(ticks) / real(room_speed);
}