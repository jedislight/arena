// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function array_find(array, value){
	var index = -1;
	for (var i = 0; i < array_length(array); ++i){
		var entry = array[i];
		if (entry == value) {
			index = i;
			break;
		}
	}
	
	return index;
}