// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function behavior_skill_spam_hostile(){
	var nearest_distance = 9999999;
	var hostile = noone;
	with(Creature) {
		if (faction != other.faction){
			var distance = distance_to_object(other.id);
			if (distance < nearest_distance){
				hostile = id;
				nearest_distance = distance;
			}
		}
	}
	if (instance_exists(hostile)){
		for (var i = 1; i < array_length(skills); ++i){
			var skill = skills[i];
			if (not instance_exists(skill)) continue;
			if (skill.cooldown_timer >0) continue;
			if (skill.projectile_speed == 0 and skill.radius > 0 and skill.radius*0.5 < distance_to_object(hostile)) continue;
			with(skill) script_execute(activate_script, other.id, hostile.x, hostile.y);
		}
	}
	
	skip_skill = irandom_range(0, instance_number(Creature));
}