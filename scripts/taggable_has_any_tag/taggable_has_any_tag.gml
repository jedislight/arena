// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function taggable_has_any_tag(taggable, tag_array){
	for (var i = 0; i < array_length(tag_array); ++i){
		if (array_find(taggable.tags, tag_array[i]) >= 0){
			return true;	
		}
	}
	
	return false;
}