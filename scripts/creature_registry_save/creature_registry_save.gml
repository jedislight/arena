// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_registry_save(filename){
	var backup_name = filename + ".backup";
	if (file_exists(backup_name)){
		file_delete(backup_name);
	}
	if(file_exists(filename)) file_rename(filename, backup_name);
	var file = file_text_open_write(filename);
	for(var creature_name = ds_map_find_first(global.creature_registry); not is_undefined(creature_name); creature_name = ds_map_find_next(global.creature_registry, creature_name)){
		var upgrade_history = global.creature_registry[? creature_name];
		file_text_write_string(file, creature_name);
		file_text_writeln(file)
		file_text_write_real(file, array_length(upgrade_history.upgrade_types));
		file_text_writeln(file)
		for(var i = 0; i < array_length(upgrade_history.upgrade_types); ++i){
			file_text_write_string(file, upgrade_history.upgrade_types[i]);	
			file_text_writeln(file)
			file_text_write_string(file, upgrade_history.upgrade_skills[i]);
			file_text_writeln(file)
			file_text_write_real(file, upgrade_history.upgrade_values[i]);
			file_text_writeln(file)
		}
	}
	file_text_close(file);
	return true;
}