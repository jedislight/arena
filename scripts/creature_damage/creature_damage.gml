// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_damage(creature, source, amount){
	if(creature_resists(creature, source.tags))
		amount *= 0.5;
	if(creature_vulnerable(creature, source.tags))
		amount *= 2.0;
	creature.hp -= amount;
	creature.hp = min(creature.hp, creature.defense);
}