// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function activate_summon(caster, xx, yy){
	if (creature_is_casting(caster)) {
		if (radius != 0) {
			var dir = point_direction(caster.x, caster.y, xx ,yy);
			xx = caster.x + lengthdir_x(radius, dir)
			yy = caster.y + lengthdir_y(radius, dir)
		}
		var summon = instance_create_depth(xx, yy,0, summon_object_index);
		skill_faction_assign(summon, caster);
		if (damage != 0){
			for(var i = 0; i < array_length(summon.skills); ++ i) {
				var skill = summon.skills[i];
				if (not instance_exists(skill)) continue
				if (skill.damage != 0)
					skill.damage = damage;	
			}
		}
		if (radius != 0){
			for(var i = 0; i < array_length(summon.skills); ++ i) {
				var skill = summon.skills[i];
				if (not instance_exists(skill)) continue
				if (skill.radius != 0)
					skill.radius = radius;	
			}
		}
		if (monosummon){
			with(summon_object_index) {
				if (faction == caster.faction and id != summon.id){
					instance_destroy(id);
				}
			}
		}
		skill_activated(id, caster);
	} else if (skill_ready(id, xx, yy)) {
		skill_begin_casting(caster, id, xx, yy);
	}
}