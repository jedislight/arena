// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function hud_draw_tooltip(heading, description, tooltip_text, tooltip_values){
	draw_set_font(TooltipFont);
	var width = string_width(tooltip_text) + string_width(tooltip_values);
	var width = max(width, 200);
	draw_set_font(TooltipHeaderFont);
	width = max(width, string_width(heading));
	var header_height = string_height_ext(heading, -1, width);
	draw_set_font(TooltipFont);
	var description_height = string_height_ext(description, -1, width);
	var detail_height = max(
		string_height_ext(tooltip_text, -1, width),
		string_height_ext(tooltip_values, -1, width)
	);
	var height = header_height + description_height + detail_height;
		
	var tooltip_x = mouse_x+16;
	var tooltip_y = mouse_y;
		
	if (tooltip_x + width > room_width){
		tooltip_x = room_width - width;
	}
	if (tooltip_y + height > room_height){
		tooltip_y = room_height - height;	
	}
		
	var left = tooltip_x;
	var top = tooltip_y;
	var right = tooltip_x + width;
	var center = tooltip_x + width/2;
	var bottom = tooltip_y + height;
		
	draw_set_color(c_black);
	draw_rectangle(left, top, right, bottom, false);
	draw_set_color(c_white);
	draw_rectangle(left, top, right, bottom, true);

	draw_set_font(TooltipHeaderFont);
	draw_set_halign(fa_center);
	draw_text(center, top, heading);
		
	draw_set_font(TooltipFont);
	draw_set_halign(fa_left);
	draw_text_ext(left, top + header_height, description, -1, width);
	draw_text(left, top + header_height + description_height, tooltip_text);
	draw_set_halign(fa_right);
	draw_text(right, top + header_height + description_height, tooltip_values);
	draw_set_halign(fa_left);
}