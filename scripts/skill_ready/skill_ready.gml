// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function skill_ready(skill, xx, yy){
	if (skill.cooldown_timer > 0.0) return false;
	return true;
}