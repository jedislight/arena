// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function menu_action_start_match(arg){
	if (RunController.match_number == 1) {
		var planner = instance_find(MatchPlanner,0);
		var player_team = planner.teams[? 1]
		for (var i = 0; i < array_length(player_team); ++i){
			if (ds_map_exists(global.creature_registry, player_team[i])) {
				ds_map_delete(global.creature_registry, player_team[i])
				global.creature_registry[? player_team[i]] = new CreatureUpgradeHistory();
			}
		}
	}
	menu_action_goto_room(MatchRoom);
}