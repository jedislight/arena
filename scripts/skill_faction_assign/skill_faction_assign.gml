// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function skill_faction_assign(object, caster){
	var faction = caster.faction;
	if (factionless){
		faction = -999;
	}
	faction_assign(object, faction);
}