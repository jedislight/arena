// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function upgrade_skill_radius(creature, value){
	var value_real = value / 100;
	skill.radius *= 1 + value_real;
}