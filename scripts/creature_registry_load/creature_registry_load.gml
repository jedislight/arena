// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_registry_load(filename){
	ds_map_clear(global.creature_registry);
	ds_map_clear(global.creature_name_to_object_index);
	{
		var base_creatures = tag_get_assets(["BASE_CREATURE"]);
		for(var i = 0; i < array_length(base_creatures); ++i){
			var asset_name = base_creatures[i];
			var object = asset_get_index(asset_name);
			var temp = instance_create_depth(0,0,0, object);
			var name = temp.name;
			show_debug_message("Initializing Creature: " + name);
			global.creature_name_to_object_index[? name] = object;
			instance_destroy(temp);
		}
	}
	var allNames = ds_map_keys_to_array(global.creature_name_to_object_index);
	for(var i = 0; i < array_length(allNames); ++i){
		if (string_contains(allNames[i], "Training Dummy")) continue
		global.creature_registry[? allNames[i]] = new CreatureUpgradeHistory();	
	}
	if (not file_exists(filename)) return false;
	var file = file_text_open_read(filename);
	while( not file_text_eof(file)) {
		var upgrade_history = new CreatureUpgradeHistory();
		var name = string_strip(file_text_readln(file));
		global.creature_registry[? name] = upgrade_history;
		
		var history_size = real(file_text_readln(file));
		
		for(var i = 0; i < history_size; ++i){
			var type = string_strip(file_text_readln(file));	
			var skill = string_strip(file_text_readln(file));
			var value = real(file_text_readln(file));
			
			array_push(upgrade_history.upgrade_types, type);
			array_push(upgrade_history.upgrade_skills, skill);
			array_push(upgrade_history.upgrade_values, value);
		}
	}
	file_text_close(file);
	return true;
}