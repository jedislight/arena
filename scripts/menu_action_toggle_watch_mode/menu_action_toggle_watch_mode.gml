// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function menu_action_toggle_watch_mode(arg){
	global.watch_mode = ! global.watch_mode;
	if (global.watch_mode){
		text = "Watch Mode:  On"	
	}
	else {
		text = "Watch Mode: Off";	
	}
}