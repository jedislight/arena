// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function array_merge(dest, source){
	for(var i = 0; i < array_length(source); ++i){
		var value = source[i];
		if (array_find(dest, value) < 0){
			array_push(dest, value);	
		}
	}
}