// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function behavior_move_to_hostile(){
	var nearest_distance = 9999999;
	var hostile = noone;
	with(Creature) {
		if (faction != other.faction){
			var distance = distance_to_object(other.id);
			if (distance < nearest_distance){
				hostile = id;
				nearest_distance = distance;
			}
		}
	}
	if (instance_exists(hostile)){
		move_to_x = hostile.x;
		move_to_y = hostile.y;
	}
	
	skip_movement = irandom_range(0, instance_number(Creature));
}