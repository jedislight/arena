// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_vulnerable(creature, tag_array){
	for (var i = 0; i < array_length(tag_array); ++i){
		if (array_find(creature.vulnerabilities, tag_array[i]) >= 0){
			return true;	
		}
	}
	
	return false;
}