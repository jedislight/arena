// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function skill_get_sprite(skill){
	var skill_sprite = noone;
	if (instance_exists(skill)){
		if (object_exists(skill.projectile_object_index))
			skill_sprite = object_get_sprite(skill.projectile_object_index);
		if (object_exists(skill.summon_object_index))
			skill_sprite = object_get_sprite(skill.summon_object_index);
		if (sprite_exists(skill.sprite_index)){
			skill_sprite = skill.sprite_index;	
		}
	}
	
	return skill_sprite;
}