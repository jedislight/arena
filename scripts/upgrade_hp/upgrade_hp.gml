// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function upgrade_hp(creature, value){
	var value_real = value / 100;
	creature.defense *= 1 + value_real;
	creature.hp = creature.defense;
}