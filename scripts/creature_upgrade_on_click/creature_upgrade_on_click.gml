// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_upgrade_on_click(){
if (image_alpha != 1.0) exit;
creature.stars += 1;
script_execute(upgrade_script, creature, value);
var upgrade_history = global.creature_registry[? creature.name];
if (is_undefined(upgrade_history)) {
	upgrade_history = new CreatureUpgradeHistory();
	global.creature_registry[? creature.name] = upgrade_history;
}
upgrade_history.append(object_index, value, skill);

creature_registry_save("main.creature_registry");

object_type = object_index;
instance_change(BloomOutFX, true);
with(CreatureUpgrade){
	if (id != other.id){
		object_type = object_index;
		instance_change(FadeAwayFX, true);
	}
}

}