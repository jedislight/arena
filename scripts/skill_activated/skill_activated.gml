// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function skill_activated(skill, caster){
	cooldown_timer = cooldown_seconds;
	caster.casting = false;
	caster.casting_activate_script = noone;
	if(skill.cancel_movement){
		caster.move_to_x = caster.x;
		caster.move_to_y = caster.y;
	}
}