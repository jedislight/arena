// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function creature_upgrade_reroll(){
	var upgrades = tag_get_assets("CREATURE_UPGRADE")
	var upgrade_type = asset_get_index(array_sample(upgrades));
	instance_change(upgrade_type, true);
}