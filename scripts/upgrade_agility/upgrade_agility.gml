// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function upgrade_agility(creature, value){
	var value_real = value / 100;
	creature.move_speed_per_second *= 1 + value_real;
	creature.turn_rate_per_second *= 1 + value_real;
}