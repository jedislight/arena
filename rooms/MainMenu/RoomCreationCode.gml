with(Skill) {
	var owned = false;
	var skill = id;
	with(Creature) {
		if (array_find(skills, skill) >= 0) {
			owned = true;	
		}
	}
	
	if (not owned) instance_destroy(id);
}