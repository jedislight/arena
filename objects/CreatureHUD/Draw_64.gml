/// @description Insert description here
// You can write your code in this editor

if (not instance_exists(creature)) exit;
draw_set_font(HUDFont);
draw_set_color(c_white);
creature_draw_healthbar(x, y, x+skillbox_size*4.5, y+skillbox_size, creature)
var annotations = ["", "Q", "W", "E", "R"];
var tooltip_index = -1;
for (var i = 1; i < array_length(creature.skills); ++i){
	// Button
	var skill = creature.skills[i];
	var top = y + skillbox_size;
	var bottom = y + skillbox_size * 2;
	var left = x + skillbox_size * i - skillbox_size*0.5;
	var right = left + skillbox_size;
	draw_sprite_stretched(SpriteFrame, 0, left, top, skillbox_size, skillbox_size);
	draw_rectangle(left, top, right, bottom, true);
	var skill_sprite = skill_get_sprite(skill);
	if (sprite_exists(skill_sprite)) {
		draw_sprite_stretched(skill_sprite, 0, left, top, skillbox_size, skillbox_size);
	}
	var cooldown_percent = 0;
	if (instance_exists(skill)) {
		cooldown_percent = skill.cooldown_timer / skill.cooldown_seconds;
	}
	draw_set_alpha(0.5);
	draw_rectangle_color(left, bottom-skillbox_size * cooldown_percent, right, bottom, c_gray, c_gray, c_gray, c_gray, false);
	draw_set_alpha(1.0);
	draw_rectangle(left, top, right, bottom, true);
	draw_text(left+2, top, annotations[i])
	
	// Tool-tip
	if (instance_exists(skill) and point_in_rectangle(mouse_x, mouse_y, left, top, right, bottom)){
		tooltip_index = i;
	}
}
if (tooltip_index >= 0) {
	var skill = creature.skills[tooltip_index];
	var tooltip_text = "";
	var tooltip_values = "";
	if (skill.damage != 0){
		if (skill.damage > 0)
			tooltip_text += "\nDamage: ";
		else
			tooltip_text += "\nHealing: ";
		tooltip_values += "\n" + string(abs(skill.damage));
	}
	if (skill.radius != 0) {
		tooltip_text += "\nRadius: ";
		tooltip_values += "\n" + string(skill.radius);
	}
	if (skill.factionless) {
		tooltip_text += "\nFactionless: ";
		tooltip_values += "\nYes";
	}
	if (object_exists(skill.projectile_object_index)){
		tooltip_text += "\nProjectile: ";
		tooltip_values += "\n" + object_get_name(skill.projectile_object_index);
		tooltip_text += "\n Speed: ";
		tooltip_values += "\n" + string(skill.projectile_speed)
		if (skill.knockback != 0){
			tooltip_text += "\n Knockback: ";
			tooltip_values += "\n" + string(skill.knockback);
		}
	}
	if (skill.cooldown_seconds != 0){
		tooltip_text += "\nCooldown: ";
		tooltip_values += "\n" + string(skill.cooldown_seconds)+"s";
	}
	if (skill.casting_time_seconds != 0){
		tooltip_text += "\nCast Time: ";
		tooltip_values += "\n" + string(skill.casting_time_seconds)+"s";
	}
	
	tooltip_text += "\nTags: ";
	for(var i = 0; i < array_length(skill.tags); ++i){
		tooltip_values += "\n" + skill.tags[i];	
	}

	hud_draw_tooltip(skill.name, skill.description, tooltip_text, tooltip_values); 
}

// Details
if (room != MainMenu) exit;

details = ""
if (array_length(creature.tags) > 0)
	details += "\nTags: " + array_to_string(creature.tags);
if (array_length(creature.resistances) > 0)
	details += "\nResist: " + array_to_string(creature.resistances);
if (array_length(creature.vulnerabilities) > 0)
	details += "\nVulnerable: " + array_to_string(creature.vulnerabilities);
if (array_length(creature.forbidden) > 0)
	details += "\nForbidden: " + array_to_string(creature.forbidden);
details += "\nSpeed: " + string(creature.move_speed_per_second) + "pps | " + string(creature.turn_rate_per_second*60) + "rpm";
details = string_strip(details);
draw_text( x+skillbox_size*4.5, y, details);