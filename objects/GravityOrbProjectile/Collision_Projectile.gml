/// @description Insert description here
// You can write your code in this editor

if (other.object_index == object_index){
	if (other.faction != faction){
		other.object_type = other.object_index;
		with(other) instance_change(BloomOutFX, true);		
		object_type = object_index;
		instance_change(BloomOutFX, true);		
	}
	else if (other.id < id){
		x = (other.x + x) / 2;
		y = (other.y + y) / 2;
		image_xscale += other.image_xscale;
		image_yscale += other.image_yscale;
		damage += other.damage;
		damage *= 1.5;
		speed *= 0.5;
		instance_destroy(other.id);
	}
	
}
else if (other.piercing == 0) {
	other.object_type = other.object_index;
	with(other) instance_change(BloomOutFX, true);
}