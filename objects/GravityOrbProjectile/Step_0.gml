/// @description Insert description here
// You can write your code in this editor
with(Projectile){
	var amount = 0.1;
	if (other.object_index != object_index) {
		amount = 0.3;	
	}
	if (id != other.id and speed != 0)
		motion_add(point_direction(x,y, other.x, other.y), amount);	
}

image_angle += 5;