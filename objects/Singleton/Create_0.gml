/// @description Insert description here
// You can write your code in this editor
persistent = true;
var duplicate = false;
with(object_index){
	if (other.id > id){
		duplicate = true;	
	}
}

if(duplicate) {
	instance_destroy(id);
	exit;
}