/// @description Insert description here
// You can write your code in this editor

//dervied metrics ever so often
if (irandom_range(0, 1000) > 1) exit;
var metrics_keys = ds_map_keys_to_array(global.metrics);
for(var i = 0; i < array_length(metrics_keys); ++i){
	var key = metrics_keys[i];
	var is_runs_key = string_contains(key, ".runs");
	if (not is_runs_key) continue;
	var dot_index = string_pos(".", key);
	var group = string_copy(key, 1, dot_index-1);
	var matches_key = group+".match_wins";
	if (not ds_map_exists(global.metrics, matches_key)) continue;
	var matches_won = global.metrics[? matches_key];
	var runs = global.metrics[? key];
	
	var match_win_ratio = matches_won/runs;
	global.metrics[? group + ".match_win_ratio"] = match_win_ratio;
}