/// @description Insert description here
// You can write your code in this editor
if (not debug_enabled) exit
	
var text = "";

text += "\nPerformance";
text += "\nFPS: " + string(fps);
text += "\nFPS(real): " + string(fps_real);
text += "\nInstances: " + string(instance_count);
text += "\n";

text += "\nMetrics"
text += "\n"+ds_map_to_string(global.metrics);
text += "\n";

draw_set_alpha(0.5);
draw_rectangle_color(0,0, room_width, room_height, c_dkgray, c_dkgray, c_dkgray, c_dkgray, false);
draw_set_alpha(1.0);
draw_text_color(0 ,y, text, c_white, c_ltgray, c_white, c_dkgray, draw_get_alpha());
