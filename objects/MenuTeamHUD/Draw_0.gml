/// @description Insert description here
// You can write your code in this editor
var unit_size = 128;
var upgrade_size = 64;
var match_planner = instance_find(MatchPlanner, 0);
if (not match_planner or is_undefined(match_planner.teams[? 1]) or array_length(match_planner.teams[? 1]) == 0){
	exit;
}

var draw_team_hud_creature = function(team_index, label, unit_size, xx ,yy){
	var match_planner = instance_find(MatchPlanner, 0);

	draw_set_font(MenuTeamsFont);
	var hero_name = match_planner.teams[? 1][team_index];
	draw_text(xx,yy, label + ": " + hero_name);	
}

draw_team_hud_creature(0, "Hero", unit_size, x, y);
if( array_length(match_planner.teams[? 1]) > 1){
	draw_team_hud_creature(1, "Ally", unit_size,x, y+unit_size);
}

