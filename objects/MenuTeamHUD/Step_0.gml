/// @description Insert description here
// You can write your code in this editor

if (instance_number(Creature) == 0){	
	var match_planner = instance_find(MatchPlanner, 0);
	for(var team_index = 0; team_index <= 1; ++team_index){
		if(array_length(match_planner.teams[? 1]) > team_index){
			var hero_name = match_planner.teams[? 1][team_index];
			var hero_object_index = creature_get_object_index_from_name(hero_name);
			var hero_sprite = object_get_sprite(hero_object_index);
			var hero_sprite_width = sprite_get_width(hero_sprite);
			var hero_sprite_height = sprite_get_height(hero_sprite);
			var scale = 128 /( (hero_sprite_width + hero_sprite_height) / 2);

			var h = instance_create_depth(x + hero_sprite_width * scale /2,y+team_index*128+hero_sprite_height * scale /2,0, hero_object_index);
			h.movement_behavior = noone;
			h.skill_behavior = noone;
			h.image_xscale = scale;
			h.image_yscale = scale;
			if (RunController.match_number > 1) {
				var upgrade_history = global.creature_registry[? hero_name];
				if (not is_undefined(upgrade_history)) {
					upgrade_history.apply(h.id, 9999);	
				}
			}
		}
	}
}

