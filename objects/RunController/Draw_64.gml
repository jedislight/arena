/// @description Insert description here
// You can write your code in this editor
draw_set_font(HUDFont);
var xx = room_width /2;
var yy = room_height;
var match_number = RunController.match_number;
var match_limit = RunController.match_limit;
var text = "Match: " + string(match_number) + " / " + string(match_limit);
draw_set_valign(fa_bottom);
draw_set_halign(fa_center);
draw_text(xx, yy, text);
draw_set_halign(fa_left);
draw_set_valign(fa_top);