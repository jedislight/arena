{
  "spriteId": {
    "name": "otherbot",
    "path": "sprites/otherbot/otherbot.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "Creature",
    "path": "objects/Creature/Creature.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [],
  "properties": [],
  "overriddenProperties": [
    {"propertyId":{"name":"faction","path":"objects/Creature/Creature.yy",},"objectId":{"name":"Creature","path":"objects/Creature/Creature.yy",},"value":"1","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"skill1","path":"objects/Creature/Creature.yy",},"objectId":{"name":"Creature","path":"objects/Creature/Creature.yy",},"value":"EtherTear","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"hp","path":"objects/Creature/Creature.yy",},"objectId":{"name":"Creature","path":"objects/Creature/Creature.yy",},"value":"1","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"defense","path":"objects/Creature/Creature.yy",},"objectId":{"name":"Creature","path":"objects/Creature/Creature.yy",},"value":"1","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"name","path":"objects/Creature/Creature.yy",},"objectId":{"name":"Creature","path":"objects/Creature/Creature.yy",},"value":"Blinkbot","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"tags","path":"objects/Taggable/Taggable.yy",},"objectId":{"name":"Taggable","path":"objects/Taggable/Taggable.yy",},"value":"\"DARK\", \"WIND\", \"MOBILITY\"","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"forbidden","path":"objects/Taggable/Taggable.yy",},"objectId":{"name":"Taggable","path":"objects/Taggable/Taggable.yy",},"value":"\"DEFENSE\"","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
  ],
  "parent": {
    "name": "Creatures",
    "path": "folders/Objects/Creatures.yy",
  },
  "resourceVersion": "1.0",
  "name": "BlinkBot",
  "tags": [
    "BASE_CREATURE",
  ],
  "resourceType": "GMObject",
}