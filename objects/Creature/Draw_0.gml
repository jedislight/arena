/// @description Insert description here
// You can write your code in this editor
var image_xscale_backup = image_xscale;
var image_yscale_backup = image_yscale;

if(creature_is_casting(id)){
	var ticks_to_cast = alarm[Creature_Alarms.CASTING];
	var scale = 1.0 + cos(ticks_to_cast/10) / 5;
	image_xscale *= scale;
	image_yscale *= scale;
}
draw_self();
image_xscale = image_xscale_backup;
image_yscale = image_yscale_backup;


if (hp != defense){
	var sprite_size = (sprite_width + sprite_height) / 2;
	var height = sprite_size / 10;
	var left = x - sprite_size / 2;
	var right = x + sprite_size / 2;
	var bottom = y - sprite_size;
	var top = bottom - height;

	creature_draw_healthbar(left, top, right, bottom, id);
}