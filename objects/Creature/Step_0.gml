/// @description Insert description here
// You can write your code in this editor
if (hp <= 0) {
	for(var i = 0; i < array_length(cast_on_death); ++i){
		alarm[Creature_Alarms.CASTING] = 0; // cast time override
		var skill_index = cast_on_death[i];
		with(skills[skill_index]) {
			script_execute(activate_script, other.id, other.x, other.y);
		}
	}
	instance_destroy(id);	
}
if (--skip_movement <= 0 and script_exists(movement_behavior)) {
	script_execute(movement_behavior);	
}

if (--skip_skill <= 0 and not creature_is_casting(id) and script_exists(skill_behavior)) {
	script_execute(skill_behavior);	
}

var xx = move_to_x;
var yy = move_to_y;
if (creature_is_casting(id)){
	xx = casting_x;
	yy = casting_y;
}
if (not collision_point(xx, yy, id, true, false)){
	// turning

	var direction_to_move = point_direction(x, y, xx, yy);
	var rotation_needed = angle_difference(direction_to_move, image_angle);
	var turn_rate_per_tick = turn_rate_per_second * 360 / room_speed;
	var turn_amount_needed = min(turn_rate_per_tick, abs(rotation_needed));
	var final_turn_amount = sign(rotation_needed) * turn_amount_needed;
	image_angle += final_turn_amount;
	
	// movement
	if (not creature_is_casting(id)){
		if (abs(rotation_needed) < 45) {
			var speed_per_tick = move_speed_per_second / room_speed;
			motion_add(direction_to_move, speed_per_tick);
		}
	}
}

for(var i = 0; i < array_length(dot_damages); ++i){
	var damage_left = dot_damages[i];
	var duration_left_ticks = dot_duration_ticks[i];
	var damage_this_tick = damage_left / duration_left_ticks;
	hp -= damage_this_tick;
	duration_left_ticks -= 1;
	damage_left -= damage_this_tick;
	if (duration_left_ticks <= 0) {
		array_delete(dot_damages, i, 1);
		array_delete(dot_duration_ticks, i, 1);
		--i;
	} else {
		dot_damages[i] = damage_left;
		dot_duration_ticks[i] = duration_left_ticks;
	}
}

if (x < 0)	x /= 2;	
if (y < 0) y /= 2;
if (x > room_width) x -= (x - room_width) / 2
if (y > room_height) y -= (y - room_height) / 2