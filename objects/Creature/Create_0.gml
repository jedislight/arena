/// @description Insert description here
// You can write your code in this editor
enum Creature_Alarms {
	CASTING = 0	
}

casting = false;
casting_x = 0;
casting_y = 0;
casting_activate_script = noone;
move_to_x = x;
move_to_y = y;
dot_damages = [];
dot_duration_ticks = [];
skip_movement_max = room_speed;
skip_skill_max = room_speed / 2;
skip_movement = 0;
skip_skill = 0;
skills = [noone, noone, noone, noone, noone];
if (object_exists(skill1)){
	skills[1] = instance_create_depth(0,0,0, skill1);	
}
if (object_exists(skill2)){
	skills[2] = instance_create_depth(0,0,0, skill2);	
}
if (object_exists(skill3)){
	skills[3] = instance_create_depth(0,0,0, skill3);	
}
if (object_exists(skill4)){
	skills[4] = instance_create_depth(0,0,0, skill4);	
}

stars = 0;

if (array_equals([0], resistances)) resistances = [];
if (array_equals([0], vulnerabilities)) vulnerabilities = [];
if (array_equals([0], tags)) tags = [];
if (array_equals([0], forbidden)) forbidden = [];