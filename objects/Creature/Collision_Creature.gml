/// @description Insert description here
// You can write your code in this editor

var eject_direction = point_direction(other.x, other.y, x, y);
var eject_distance = max(speed, other.speed, 1);
x += lengthdir_x(eject_distance, eject_direction);
y += lengthdir_y(eject_distance, eject_direction);