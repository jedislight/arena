/// @description Creature_Alarms.CASTING

var skill = skills[casting_skill_index];
with(skill) script_execute(activate_script, other.id, other.casting_x, other.casting_y);

casting = false;
casting_x = 0;
casting_y = 0;
casting_skill_index = -1;