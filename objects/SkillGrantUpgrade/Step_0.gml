/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
var any_slot_empty = false;
for(var i = 1; i < array_length(creature.skills); ++i) {
	if (not instance_exists(creature.skills[i]))
		any_slot_empty = true;
	if (instance_exists(creature.skills[i]) and creature.skills[i].object_index == skill){
		// skill already known
		creature_upgrade_reroll();
		break;
	}
}

// no slots available
if (not any_slot_empty){
	creature_upgrade_reroll()	
}