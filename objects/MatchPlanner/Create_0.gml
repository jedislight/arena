/// @description Insert description here
// You can write your code in this editor
teams  = ds_map_create()

var allRegistryCreatureNames = ds_map_keys_to_array(global.creature_registry);

teams[? 1] = [array_sample(allRegistryCreatureNames)];

do{
	teams[? 1][1] = array_sample(allRegistryCreatureNames);
} until (teams[? 1][0] != teams[? 1][1])

if(RunController.match_number > 1 and array_length(RunController.run_team) > 0){
	teams[?1] = RunController.run_team;	
}

starting_match_number = RunController.match_number

star_target= max(0,starting_match_number - 1);
global.star_cap = star_target;
star_target = power(star_target, 1.5);

var max_registered_stars = 0;
for(var key = ds_map_find_first(global.creature_registry); not is_undefined(key); key = ds_map_find_next(global.creature_registry, key)) {
	max_registered_stars = max(max_registered_stars, array_length(global.creature_registry[? key].upgrade_types));	
}
if (max_registered_stars == 0){
	teams[? 2] = ["Training Dummy"];
	exit;
}

var availableEnemyCreatures = [];
for(var i = 0; i < array_length(allRegistryCreatureNames); ++i){
	var registryName = allRegistryCreatureNames[i];
	if (array_find(teams[? 1], registryName) < 0 and registryName != "Training Dummy"){
		array_push(availableEnemyCreatures, registryName);	
	}
}

teams[? 2] = [];
do{
	var creature_name = array_sample(availableEnemyCreatures);
	var creature_stars = 1+max(1.0, array_length(global.creature_registry[? creature_name].upgrade_types));
	var extra_fill = random_range(0.0, 1.0) * star_target;
	var creature_count = floor(1 +  extra_fill / creature_stars);
	repeat(creature_count)
		array_push(teams[? 2], creature_name);
	star_target -= creature_count * creature_stars;
} until(star_target <= 0);