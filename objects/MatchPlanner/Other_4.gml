/// @description Insert description here
// You can write your code in this editor
if(not asset_has_any_tag(room, ["ARENA"], asset_room)){
	exit;	
}

// move existing creatures to faction spawn
with (Creature) {
	with (TeamSpawnAnchor) {
		if (other.faction == faction){ 
			other.x = x;
			other.y = y;
		}
	}
}

// spawn all team members
for (var team_faction = ds_map_find_first(teams); not is_undefined(team_faction); team_faction = ds_map_find_next(teams, team_faction)){
	var team_array = teams[? team_faction];
	for(var i = 0; i < array_length(team_array); ++i){
		var spawn_creature_name = team_array[i]
		var spawn_object_index = creature_get_object_index_from_name(spawn_creature_name);
		var spawn_x = room_width /2;
		var spawn_y = room_height /2;
		with (TeamSpawnAnchor) {
			if (team_faction == faction){ 
				spawn_x = x;
				spawn_y = y;
			}
		}
		
		var creature = instance_create_depth(spawn_x, spawn_y, 0, spawn_object_index);		
		creature.faction = team_faction;
		if(team_faction == 1 and i == 0){
			// player hero
			if (not global.watch_mode){
				creature.movement_behavior = behavior_mouse_movement;
				creature.skill_behavior = behavior_skill_qwer;
			}
			var creature_hud = instance_find(CreatureHUD, 0);
			if (instance_exists(creature_hud)){
				creature_hud.creature = creature;	
			}
		}
		
		var upgrade_history = global.creature_registry[? spawn_creature_name];
		var star_cap = global.star_cap;
		if (team_faction == 1) star_cap = 9999;
		if (not is_undefined(upgrade_history))
			upgrade_history.apply(creature.id, star_cap);
	}
}

// adjust creatures to not intersect (best attempt)
repeat(5) with(Creature) {
	if (not place_empty(x, y, Creature)) {
		x += irandom_range(-64, 64)	
		y += irandom_range(-64, 64)
	}
}

// set run's team
RunController.run_team = teams[? 1];	

// set team's metrics id
global.team_metrics_key = array_to_string(teams[? 1])
// destroy self
instance_destroy(id);