/// @description Insert description here
// You can write your code in this editor

oversat += 0.01;
if (oversat > 1.0)
	instance_destroy(id);

shader_set(BloomOutShader);

shader_set_uniform_f(shader_get_uniform(shader_current(), "amount"), oversat);
sprite_index = object_get_sprite(object_type);
event_perform_object(object_type, ev_draw, 0);
shader_reset();