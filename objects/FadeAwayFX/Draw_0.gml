/// @description Insert description here
// You can write your code in this editor

image_alpha *= .95;
if (image_alpha < 0.10)
	instance_destroy(id);
	
if (shrink){
	image_xscale *= .95;
	image_yscale *= .95;
}

sprite_index = object_get_sprite(object_type);
event_perform_object(object_type, ev_draw, 0);