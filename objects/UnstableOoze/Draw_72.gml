/// @description Insert description here
// You can write your code in this editor
event_inherited();

wobble_x += random_range(-0.01, 0.01);
wobble_y += random_range(-0.01, 0.01);

wobble_x = clamp(wobble_x, -0.2, 0.2);
wobble_y = clamp(wobble_y, -0.2, 0.2);

image_xscale += wobble_x;
image_yscale += wobble_y;