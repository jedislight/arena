/// @description Insert description here
// You can write your code in this editor
duration_ticks = seconds_to_ticks(casting_time_seconds);
if (instance_exists(caster)){
	x = caster.x;
	y = caster.y;
}
age += 1;

intensity = lerp(intensity_start, intensity_end, age/duration_ticks);
size = lerp(size_start, size_end, age/duration_ticks);

if (random_range(0.0,1.0) < intensity) {
	var n = instance_create_depth(x,y,-999, FadeAwayFX)	
	n.image_angle = irandom_range(0,360);
	var scale = random_range(-0.1,0.1)+size;
	scale = clamp(scale, 0.0, 1.0);
	scale *= 5;
	n.object_type = object_index;
	n.image_xscale = scale;
	n.image_yscale = scale;
	n.image_alpha = intensity;
	n.shrink = true;
}

if (not instance_exists(caster)) {
	instance_destroy(id);	
}