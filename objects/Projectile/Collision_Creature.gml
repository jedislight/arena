/// @description Insert description here
// You can write your code in this editor

if (faction != other.faction) {
	if (damage_duration_seconds == 0.0) {
		creature_damage(other.id, id, damage);
	} else {
		creature_add_dot(other.id, id,  damage, damage_duration_seconds);	
	}
	if(knockback != 0){
		var knockback_dir = direction;
		var knockback_amount = knockback;
		
		var kx = lengthdir_x(knockback_amount, knockback_dir);
		var ky = lengthdir_y(knockback_amount, knockback_dir);
		other.x += kx;
		other.y += ky;
	}
	piercing -= 1;
	if (piercing <= 0)
		instance_destroy(id);
}