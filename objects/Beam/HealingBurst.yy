{
  "spriteId": null,
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "Skill",
    "path": "objects/Skill/Skill.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [],
  "properties": [
    {"varType":0,"value":"1","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[],"resourceVersion":"1.0","name":"cooldown_seconds","tags":[],"resourceType":"GMObjectProperty",},
    {"varType":0,"value":"20","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[],"resourceVersion":"1.0","name":"projectile_speed","tags":[],"resourceType":"GMObjectProperty",},
    {"varType":0,"value":"1","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[],"resourceVersion":"1.0","name":"damage","tags":[],"resourceType":"GMObjectProperty",},
    {"varType":5,"value":"Projectile","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[
        "GMObject",
      ],"resourceVersion":"1.0","name":"projectile_object_index","tags":[],"resourceType":"GMObjectProperty",},
    {"varType":5,"value":"activate_shoot_projectile","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[
        "GMScript",
      ],"resourceVersion":"1.0","name":"activate_script","tags":[],"resourceType":"GMObjectProperty",},
    {"varType":2,"value":"Skill Name","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[],"resourceVersion":"1.0","name":"name","tags":[],"resourceType":"GMObjectProperty",},
    {"varType":2,"value":"Healing energy surounds the caster, restoring health to anyone withing a radius.","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[],"resourceVersion":"1.0","name":"description","tags":[],"resourceType":"GMObjectProperty",},
  ],
  "overriddenProperties": [
    {"propertyId":{"name":"cooldown_seconds","path":"objects/Skill/Skill.yy",},"objectId":{"name":"Skill","path":"objects/Skill/Skill.yy",},"value":"10","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"projectile_speed","path":"objects/Skill/Skill.yy",},"objectId":{"name":"Skill","path":"objects/Skill/Skill.yy",},"value":"0","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"damage","path":"objects/Skill/Skill.yy",},"objectId":{"name":"Skill","path":"objects/Skill/Skill.yy",},"value":"-5","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"projectile_object_index","path":"objects/Skill/Skill.yy",},"objectId":{"name":"Skill","path":"objects/Skill/Skill.yy",},"value":"Burst","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"name","path":"objects/Skill/Skill.yy",},"objectId":{"name":"Skill","path":"objects/Skill/Skill.yy",},"value":"Healing Burst","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"factionless","path":"objects/Skill/Skill.yy",},"objectId":{"name":"Skill","path":"objects/Skill/Skill.yy",},"value":"True","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"radius","path":"objects/Skill/Skill.yy",},"objectId":{"name":"Skill","path":"objects/Skill/Skill.yy",},"value":"128","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"tags","path":"objects/Taggable/Taggable.yy",},"objectId":{"name":"Taggable","path":"objects/Taggable/Taggable.yy",},"value":"\"LIGHT\", \"SPELL\"","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
  ],
  "parent": {
    "name": "Skills",
    "path": "folders/Objects/Skills.yy",
  },
  "resourceVersion": "1.0",
  "name": "HealingBurst",
  "tags": [
    "SKILL",
  ],
  "resourceType": "GMObject",
}