/// @description Insert description here
// You can write your code in this editor
creature = instance_sample(Creature);
if (instance_exists(creature)){
	skill = creature_sample_skill(creature);
} else {
	skill = noone;
	instance_destroy(id);
	exit;
}
value = irandom_range(value_min, value_max);
mask_index = object_get_sprite(CreatureUpgrade);
if (object_index == CreatureUpgrade) {
	creature_upgrade_reroll();
}
image_alpha = 0.0;

favored_rerolls = 1;
rerolling = true;
finalized = false;
skill_related = false;
alarm[0] = 5;