/// @description Insert description here
// You can write your code in this editor
base_sprite = object_get_sprite(CreatureUpgrade);
draw_set_alpha(image_alpha);
draw_sprite(base_sprite, 0, x, y);
var target_sprite = noone;
if (instance_exists(skill) and string_contains(name, skill.name)){
	target_sprite = skill_get_sprite(skill);
}

if (not sprite_exists(target_sprite)){
	if (instance_exists(creature))
		target_sprite = creature.sprite_index;	
}

if (sprite_exists(target_sprite))
	draw_sprite_stretched(target_sprite, 0 , x, y, sprite_get_width(base_sprite), sprite_get_height(base_sprite));	
if (instance_exists(creature) and target_sprite != creature.sprite_index)
	draw_sprite_stretched(creature.sprite_index, 0 , x, y, sprite_get_width(base_sprite)/2, sprite_get_height(base_sprite)/2);	
if (sprite_exists(sprite_index))
	draw_sprite_stretched(sprite_index, 0, x, y, sprite_get_width(base_sprite), sprite_get_height(base_sprite) );
	
draw_set_alpha(1.0);