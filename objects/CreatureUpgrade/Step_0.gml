/// @description Insert description here
// You can write your code in this editor
if (instance_exists(skill)) {
	name = string_replace(name, "$skill.name", skill.name);	
	description = string_replace(description, "$skill.name", skill.name);	
}
else if (object_exists(skill)){
	var n = instance_create_depth(0,0,0, skill);
	name = string_replace(name, "$skill.name", n.name);	
	description = string_replace(description, "$skill.name", n.name);	
	
	description = string_replace(description, "$skill.description", n.description);	
	
	instance_destroy(n);
}
var is_skill_updgrade = not object_exists(skill) and instance_exists(skill) and string_contains(name, skill.name);
var is_skill_grant = object_exists(skill);
skill_related = is_skill_updgrade or is_skill_grant;
if (creature_forbids(creature, tags)){
	creature_upgrade_reroll();	
}

if (not is_skill_grant and favored_rerolls > 0 and not (taggable_has_any_tag(creature, tags) or (is_skill_updgrade and taggable_has_any_tag(creature, skill.tags)))){
	creature_upgrade_reroll();
	favored_rerolls -= 1;	
}

image_alpha += 1.0 / seconds_to_ticks(fade_in_time_seconds);
image_alpha = clamp(image_alpha, 0.0, 1.0);

if (global.watch_mode and finalized){
	if (random_range(0.0, 1.0) < 0.01)
		creature_upgrade_on_click();
}