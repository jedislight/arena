/// @description Insert description here
// You can write your code in this editor
finalized = true;
if(skill_related){
	var n = skill;
	var destroy = false;
	if (not instance_exists(n)){
		n = instance_create_depth(0,0,0, skill)	
		destroy = true;
	}
	taggable_inherit(id, n)
	if (destroy) instance_destroy(n);
}