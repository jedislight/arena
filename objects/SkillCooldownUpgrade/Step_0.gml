/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if (not instance_exists(skill) or seconds_to_ticks(skill.cooldown_seconds) <= 1.0 or skill.cooldown_seconds == 0 or skill.cooldown_seconds <= skill.casting_time_seconds)
	creature_upgrade_reroll();