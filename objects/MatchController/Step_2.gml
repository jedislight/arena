/// @description Insert description here
// You can write your code in this editor
var center = room_width / 2;
var middle = room_height / 2;

var faction_creature_counts = ds_map_create();
with(Creature){
	if (not temporary){
		if (ds_map_exists(faction_creature_counts, faction))
			faction_creature_counts[? faction] += 1;
		else 
			faction_creature_counts[? faction] = 1;
	}
}
var factions = [];
ds_map_keys_to_array(faction_creature_counts, factions);
if(array_length(transition_on_missing) == 0)
{
	if (not ds_map_exists(faction_creature_counts, victory_faction)) {
		// loss
		match_controller_disable_creatures();
		var loss_button = instance_create_depth(center, middle, -999, Button);
		loss_button.text = "Defeat";
		loss_button.action = menu_action_goto_room;
		loss_button.action_arg = MainMenu;
		array_push(transition_on_missing, loss_button);
		RunController.match_number = 0;
		if(global.watch_mode)
			instance_destroy(loss_button);
		metrics_increment(global.team_metrics_key+".runs", 1)
	} else if (array_length(factions) == 1) {
		// win
		match_controller_disable_creatures()

		if (RunController.match_number == RunController.match_limit){
			var win_button = instance_create_depth(center, middle, -999, Button);
			win_button.text = "Win";
			win_button.action = menu_action_goto_room;
			win_button.action_arg = MainMenu;
			array_push(transition_on_missing, win_button);
			RunController.match_number = 0;
			if (global.watch_mode) {
				instance_destroy(win_button);
			}
			metrics_increment(global.team_metrics_key+".runs", 1)
			metrics_increment(global.team_metrics_key+".wins", 1)
		} else {
			var frame_width = sprite_get_width(object_get_sprite(CreatureUpgrade))
			var frame_height = sprite_get_width(object_get_sprite(CreatureUpgrade))
	
			array_push(transition_on_missing, instance_create_depth(center, middle, -999, CreatureUpgrade));
			array_push(transition_on_missing, instance_create_depth(center-frame_width, middle, -999, CreatureUpgrade));
			array_push(transition_on_missing, instance_create_depth(center-frame_width, middle-frame_height, -999, CreatureUpgrade));
			array_push(transition_on_missing, instance_create_depth(center, middle-frame_height, -999, CreatureUpgrade));
			
			metrics_increment(global.team_metrics_key+".match_wins", 1)
		}
	}
}
ds_map_destroy(faction_creature_counts);

var found = 0;
for(var i = 0; i < array_length(transition_on_missing); ++i) {
	var instance = transition_on_missing[i];
	if (instance_exists(instance)){
		found += 1;
	}
}
if (found == 0 and array_length(transition_on_missing) > 0){
	room_goto(MainMenu);		
}