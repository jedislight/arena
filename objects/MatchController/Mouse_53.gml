/// @description Insert description here
// You can write your code in this editor
if (array_length(transition_on_missing) == 0) exit;

var creature = collision_point(mouse_x, mouse_y, Creature, true, false);
if (instance_exists(creature)){
	var hud = instance_find(CreatureHUD, 0);
	if (instance_exists(hud)){
		hud.creature = creature;	
	}
}