//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float amount;

void main()
{
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	float final_amount = 1.0 + amount;
	final_amount *= final_amount;
	gl_FragColor.rgb *= vec3(final_amount, final_amount, final_amount);
}
